from google.appengine.ext.db import Model, StringProperty, DateProperty, BooleanProperty, TimeProperty, IntegerProperty


class Channel(Model):
    name = StringProperty(required=True)
    kind = StringProperty(required=True)
    description = StringProperty()
    logo = StringProperty()
    country = StringProperty()

    def __init__(self, name=None, kind=None, description=None, logo=None, country=None):
        super(Channel, self).__init__()
        self.name = name
        self.kind = kind
        self.description = description
        self.logo = logo
        self.country = country


class ChannelPrograms(Model):
    name = StringProperty(required=True)
    genre = StringProperty(required=True)
    description = StringProperty()
    date = DateProperty(required=True)
    start_time = TimeProperty(required=True)
    end_time = TimeProperty(required=True)
    image_link = StringProperty()
    premiere = BooleanProperty()
    relevance = IntegerProperty(default=0)

    def __init__(self, name=None, genre=None, description=None, date=None, start_time=None, end_time=None,
                 image_link=None, premiere=False, relevance=0):
        super(ChannelPrograms, self).__init__()
        self.name = name
        self.genre = genre
        self.description = description
        self.date = date
        self.start_time = start_time
        self.end_time = end_time
        self.image_link = image_link
        self.premiere = premiere
        self.relevance = self.set_relevance(relevance)

    def set_relevance(self, relevance):
        relevance = [relevance + 1 for item in [self.image_link, self.premiere] if item]
        return relevance
